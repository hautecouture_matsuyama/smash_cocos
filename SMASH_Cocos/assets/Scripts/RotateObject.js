

cc.Class({
    extends: cc.Component,

    properties: {
        speed: 90,
        game: null
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        this.game = cc.find('Canvas/GameManager').getComponent('GameManager');
    },

    update(dt) {
        if (this.game.isGameOver == false)
            this.node.rotation += this.speed * dt;
    },
});
