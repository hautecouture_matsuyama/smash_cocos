
cc.Class({
    extends: cc.Component,

    properties: {

    },

    // onLoad () {},

    start() {
        if (this.node.name == 'Explotion') {
            var CM = cc.find('Canvas/_Game/ColorManager').getComponent('ColorManager');
            var newColor = CM.getCurrentColor();
            var particle = this.node.getComponent(cc.ParticleSystem);
            particle.startColor = newColor;
            particle.endColor = newColor;
            particle.resetSystem();
        }
        else if (this.node.name == 'Enemy') {
            var CM = cc.find('Canvas/_Game/ColorManager').getComponent('ColorManager');
            var newColor = CM.getCurrentColor();
            this.node.color = newColor;

            this.node.runAction(cc.tintTo(0.5, CM.getNowColor()));
        }
    },

    discoloration(newColor) {
        this.node.runAction(cc.tintTo(0.5, newColor));
    },

    // update (dt) {},
});
