
var Player = require('Player');

//IDを直入力しないとエラーが発生するので注意。

var preloadedRewardedVideo = null;

cc.Class({
    extends: cc.Component,

    properties: {
        player: Player,
    },

    // onLoad () {},

    // start() {},

    //広告情報の設定
    initialize() {
        var self = this;

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            return;
        }

        var canvas = cc.find('Canvas').getComponent('CanvasManager');

        FBInstant.getRewardedVideoAsync(
            '760388657694920_760389701028149',
        ).then(function (rewarded) {
            self.preloadedRewardedVideo = rewarded;
            return self.preloadedRewardedVideo.loadAsync();
        }).then(function () {
            console.log('Video：Initialize success.');
            canvas.setPossible(null, true);
        }).catch(function (err) {
            console.log('Video：Initialize failed.')
            canvas.setPossible(null, false);
        });
    },

    //広告表示
    showAd() {
        var self = this;
        var canvas = cc.find('Canvas').getComponent('CanvasManager');

        //PCかスマホかで処理を分ける。
        if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/i)) {

            if (self.preloadedRewardedVideo == null || typeof self.preloadedRewardedVideo == 'undefined')
                return;

            self.preloadedRewardedVideo.showAsync()
                .then(function () {
                    console.log('表示成功');
                    this.player.setMuteki();
                    canvas.removeFilter();
                })
                .catch(function (e) {
                    console.log(e.message);
                    var adInter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
                    console.log('動画表示不可。代わりにインタースティシャルを表示させます。');
                    adInter.showAd(true);
                });
        }
        //PCでは動画が表示できないので代わりにインタースティシャルを表示させる。報酬付き。
        else {
            var adInter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
            adInter.showAd(true);
        }
    },

    // update (dt) {},
});
