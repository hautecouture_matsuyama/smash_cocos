

cc.Class({
    extends: cc.Component,

    properties: {
        endPos: cc.Vec2,
        isHit: false,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        this.node.runAction(cc.moveTo(0.25, this.endPos));
    },

    hitOnPlayer() {
        this.isHit = true;
        this.node.parent.getComponent('ItemManager').allCheck();
        var move = cc.moveTo(2, cc.p(0, 640));
        var fade = cc.fadeOut(2);
        this.node.runAction(cc.spawn(move, fade));
    },

    getIsHit() {
        return this.isHit;
    },

    stopping() {
        this.node.stopAllActions();
        this.node.getComponent(cc.Animation).stop();
    }

    // update (dt) {},
});
