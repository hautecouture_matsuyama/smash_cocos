
cc.Class({
    extends: cc.Component,

    properties: {

        isMove: false,
        dist: cc.Vec2,

        speed: Number,
        isAvoid: false,
        isGameOver: false,
    },

    // onLoad () {},

    start() {
        this.speed = Math.random() * (3 - 1.7) + 1.7;
        this.node.scale = Math.random() * (1.2 - 0.8) + 0.8;
    },

    setDistnation(pos) {
        this.dist = pos;
    },

    startMove() {
        var move = cc.moveTo(1, this.dist).easing(cc.easeOut(3));
        var callback = cc.callFunc(this.moveEnd, this);

        this.node.runAction(cc.sequence(move, callback));
    },

    moveEnd() {
        this.isMove = true;
    },

    stopMotion() {
        this.isGameOver = true;
        this.node.stopAllActions();
    },

    avoid() {
        //回避行動
        if (!this.isMove || this.isAvoid || this.isGameOver)
            return;

        this.isAvoid = true;

        var x = this.node.x * 1.2 - this.node.x;
        var y = this.node.y * 1.2 - this.node.y;
        this.node.runAction(cc.moveBy(0.15, cc.p(x, y)));
        setTimeout(() => {
            if (this.node == null)
                return;

            x = this.node.x - this.node.x / 1.21;
            y = this.node.y - this.node.y / 1.21;
            this.node.runAction(cc.moveBy(0.15, cc.p(-x, -y)));
            setTimeout(() => {
                if (this.node != null)
                    this.isAvoid = false;;
            }, 300);
        }, 300);
    },

    update(dt) {
        if (this.node.name == 'demoEnemy') {
            this.node.position = cc.pRotateByAngle(this.node.position, cc.p(0, 0), this.speed * dt / 3);
            return;
        }
        if (this.isMove && !this.isGameOver)
            this.node.position = cc.pRotateByAngle(this.node.position, cc.p(0, 0), this.speed * dt);
    },
});
