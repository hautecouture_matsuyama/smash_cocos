
//  IDの直入力でないとエラーが発生するので注意。

var preloadedInterstitial = null;

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // onLoad() {},

    // start() {
    //     this.initialize();
    // },

    initialize() {
        var self = this;

        var canvas = cc.find('Canvas').getComponent('CanvasManager');

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            return;
        }

        FBInstant.getInterstitialAdAsync(
            '760388657694920_760389641028155',
        ).then(function (interstitial) {
            self.preloadedInterstitial = interstitial;
            return self.preloadedInterstitial.loadAsync();
        }).then(function () {
            console.log('Interstitial：success.');
            canvas.setPossible(true, null);
        }).catch(function (err) {
            console.log('Interstitial：failed.');
            canvas.setPossible(false, null);
        });


    },

    showAd(isReward) {
        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            console.log('Display failed.');
            return;
        }

        var self = this;
        var canvas = cc.find('Canvas').getComponent('CanvasManager');

        if (self.preloadedInterstitial == null || typeof self.preloadedInterstitial == 'undefined')
            return;

        self.preloadedInterstitial.showAsync()
            .then(function () {
                console.log('Interstitial：Success!');
                if (isReward == true) {
                    var player = cc.find('Canvas/_Game/Player').getComponent('Player');
                    player.setMuteki();
                    canvas.removeFilter();
                }
            })
            .catch(function (e) {
                canvas.removeFilter();
                console.log(e.message);
            });
    }

    // update (dt) {},
});
