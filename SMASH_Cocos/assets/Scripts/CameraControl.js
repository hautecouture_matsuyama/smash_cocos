


cc.Class({
    extends: cc.Component,

    properties: {
        camera: cc.Camera,
    },

    // onLoad () {},

    onEnable() {
        var delay = cc.delayTime(0.25);
        var zoomOut = cc.scaleTo(0.15, 0.8).easing(cc.easeOut(2));
        var zoomIn = cc.scaleTo(0.9, 30).easing(cc.easeIn(2));
        this.node.runAction(cc.sequence(delay, zoomOut, zoomIn))
    },

    start() {
    },

    update(dt) {
        this.camera.zoomRatio = this.node.scale;
    },
});
