
var UserData = {
    bestScore: 0,
    lastScore: 0,
    point: 0,
};

cc.Class({
    extends: cc.Component,

    properties: {

        now: 0,
        point: 0,
        nowLabel: cc.Label,

        bestLabel: cc.Label,
        lastLabel: cc.Label,
        pointLabel: cc.Label,
        pointLabel2: cc.Label,

        _userData: null,

        isNewRecord: false,

        skinNo: 0,
        rate: 0,
    },

    // onLoad () {},

    start() {
        this.getScore();

        this.bestLabel.string = this._userData.bestScore;
        this.lastLabel.string = this._userData.lastScore;
        this.pointLabel.string = this._userData.point;
        this.pointLabel2.string = this._userData.point + 'pt';
    },

    setSkinRate(rate) {
        this.rate = rate;
    },

    countScore(value) {

        //通常ボーナス
        var enemyLength = cc.find('Canvas/_Game/Obstacles').childrenCount;
        if (enemyLength >= 5) {
            value++;
            if (enemyLength >= 10)
                value++;
        }

        this.now += value;
        this.nowLabel.string = this.now;


        //スキンボーナス
        value *= this.rate;
        this.point += value;

        if (this.now > this._userData.bestScore && this.isNewRecord == false) {
            var sound = cc.find('Canvas').getComponent('SoundManager');
            sound.playSE(7);
            this.isNewRecord = true;
        }
    },

    getScore() {
        var data = cc.sys.localStorage.getItem('UserData');

        if (data != null) {
            this._userData = JSON.parse(data);
        }
        else {
            this._userData = UserData;
        }
    },
    setScore() {
        setTimeout(() => {

            if (this.now > this._userData.bestScore) {
                this._userData.bestScore = this.now;

                //ランキングスクリプトにデータを渡す

                var leaderBoard = cc.find('Canvas/leader_bord').getComponent('Leaderboard');
                leaderBoard.sendScore(this.now);
            }

            this._userData.point += this.point;
            this._userData.point = Math.floor(this._userData.point);

            if (this._userData.point > 99999)
                this._userData.point = 99999;

            this._userData.lastScore = this.now;
            cc.sys.localStorage.setItem('UserData', JSON.stringify(this._userData));

        }, 100);
    },

    bought(value) {
        this._userData.point -= value;
        this.pointLabel.string = this._userData.point;
        this.pointLabel2.string = this._userData.point + 'pt';
        cc.sys.localStorage.setItem('UserData', JSON.stringify(this._userData));
    },

    // update (dt) {},
});
