
var ItemObject = require('ItemObject');

cc.Class({
    extends: cc.Component,

    properties: {

        items: {
            default: [],
            type: ItemObject
        },
    },

    // onLoad () {},

    start() {
    },

    allCheck() {
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].getIsHit() == false)
                return;
        }
        var GS = cc.find('Canvas/GameManager').getComponent('GenerateSystem');
        GS.generateItem();
        var CM = cc.find('Canvas/_Game/ColorManager').getComponent('ColorManager');
        CM.colorChange();

        var sound = cc.find('Canvas').getComponent('SoundManager');
        sound.playSE(5);

        setTimeout(() => {
            if (this.node != null)
                this.node.destroy();
        }, 4000);
    },

    stopChildren() {
        for (var i = 0; i < this.items.length; i++) {
            this.items[i].stopping();
        }
    }

    // update (dt) {},
});
