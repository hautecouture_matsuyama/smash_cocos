
cc.Class({
    extends: cc.Component,

    properties: {

        particlePrefab: cc.Prefab,
        obstaclePrefab: cc.Prefab,
        itemPrefab: cc.Prefab,

        obstacleParent: cc.Node,
        particleParent: cc.Node,
        itemParent: cc.Node,

        frameParent: cc.Node,

        generatePosition: {
            default: [],
            type: cc.Vec2
        },
        distnation: {
            default: [],
            type: cc.Vec2
        },
    },

    // onLoad () {},

    start() {

    },

    generatePlayerVisual(prefab) {
        var obj = cc.instantiate(prefab);
        obj.parent = this.frameParent;
    },

    generateObstacle() {
        var obj = cc.instantiate(this.obstaclePrefab);
        obj.parent = this.obstacleParent;

        var rand = Math.floor(Math.random() * 12);
        obj.setPosition(this.generatePosition[rand]);
        var OM = obj.getComponent('ObstacleManager');
        OM.setDistnation(this.distnation[rand]);
        OM.startMove();
    },

    generateItem() {
        var obj = cc.instantiate(this.itemPrefab);
        obj.parent = this.itemParent;
    },

    generateParticle(pos) {
        var obj = cc.instantiate(this.particlePrefab);
        obj.parent = this.particleParent;
        obj.setPosition(pos);

    },

    // update (dt) {},
});
