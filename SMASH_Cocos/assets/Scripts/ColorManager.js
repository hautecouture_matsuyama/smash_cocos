

var ColorObject = require('ColorObject');

cc.Class({
    extends: cc.Component,

    properties: {

        //A：サークル
        colorA: {
            default: [],
            type: cc.Color
        },
        //B：障害物
        colorB: {
            default: [],
            type: cc.Color
        },

        circle: ColorObject,

        obstacleParent: cc.Node,

        colorNum: 0,
    },

    // onLoad () {},

    start() {
        this.colorChange();
    },

    colorChange() {

        var obstacles = this.obstacleParent.children;

        var randColor = Number;
        do {
            randColor = Math.floor(Math.random() * 4);
        } while (randColor == this.colorNum);

        this.circle.discoloration(this.colorA[randColor]);

        this.node.runAction(cc.tintTo(0.5, this.colorB[randColor]));

        if (obstacles.length > 0) {
            for (var i = 0; i < obstacles.length; i++) {
                var CO = obstacles[i].getComponent('ColorObject');
                CO.discoloration(this.colorB[randColor]);
            }
        }


        this.colorNum = randColor;
    },

    getCurrentColor() {
        return this.node.color;
    },

    getNowColor() {
        return this.colorB[this.colorNum];
    }

    // update (dt) {},
});
