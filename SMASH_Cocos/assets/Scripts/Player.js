
var SoundManager = require('SoundManager');

cc.Class({
    extends: cc.Component,

    properties: {
        canJump: true,
        isJump: false,
        isSmash: false,
        isDead: false,
        isMuteki: false,

        shield: cc.Node,

        sound: SoundManager,
    },

    // onLoad () {},

    start() {
        cc.director.getCollisionManager().enabled = true;
    },

    jump() {
        if (!this.canJump || this.isDead)
            return;
        this.canJump = false;
        this.isJump = true;
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        game.allAvoid();

        var jump = cc.moveTo(0.1, cc.p(-this.node.x, -this.node.y));
        var callback = cc.callFunc(this.jumpEnd, this);

        this.node.runAction(cc.sequence(jump, callback));
        setTimeout(() => {
            this.isSmash = true;
        }, 30);
    },

    jumpEnd() {
        this.isJump = false;
        this.sound.playSE(1);

        setTimeout(() => {
            this.isSmash = false;
            setTimeout(() => {
                this.canJump = true;
            }, 40);
        }, 15);
    },

    update(dt) {
        if (!this.isJump && !this.isDead)
            this.node.position = cc.pRotateByAngle(this.node.position, cc.p(0, 0), 2.6 * dt);
    },

    setMuteki() {
        this.isMuteki = true;
        this.shield.active = true;
    },

    damage() {
        if (this.isDead)
            return;

        if (this.isMuteki) {
            this.isMuteki = false;
            this.shield.active = false;
            this.sound.playSE(6);
            return;
        }
        this.node.stopAllActions();
        this.isDead = true;
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        game.gameOver();

        setTimeout(() => {
            this.node.getComponent(cc.Animation).play();
        }, 50);
    },

    onCollisionEnter(other, self) {
        if (other.node.name == 'Enemy') {
            if (this.isSmash && other != null) {
                var GS = cc.find('Canvas/GameManager').getComponent('GenerateSystem');
                GS.generateParticle(other.node.getPosition());
                other.node.destroy();
                this.sound.playSE(2);
            }
            else {
                this.damage();
            }
        }
        else if (other.node.name == 'point') {
            var IO = other.node.getComponent('ItemObject');
            if (IO.getIsHit() == false) {
                this.sound.playSE(0);
                IO.hitOnPlayer();
                var score = cc.find('Canvas/ScoreManager').getComponent('ScoreManager');
                score.countScore(1);
            }
        }
    }
});
