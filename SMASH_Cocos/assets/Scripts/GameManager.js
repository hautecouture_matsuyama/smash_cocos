var Player = require('Player')
var SoundManager = require('SoundManager');
var SkinManager = require('SkinManager');

cc.Class({
    extends: cc.Component,

    properties: {
        player: Player,
        isStart: false,
        isGameOver: false,

        sound: SoundManager,
        skin: SkinManager,

        gameNode: cc.Node,
        generateTime: 0,
    },

    onLoad() {
        this.setTouchEvent();
    },

    setTouchEvent() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouch: true,

            onTouchBegan: function (touch, event) {
                self.setJump();
                return false;
            }
        }, self.node);
    },

    start() {
    },
    setJump() {
        if (this.isStart && !this.isGameOver) {
            this.player.jump();
        }
    },

    gameStart() {

        this.skin.dicisionSkin();

        this.setTouchEvent();
        var GS = cc.find('Canvas/GameManager').getComponent('GenerateSystem');
        GS.generateItem();
        var canvas = cc.find('Canvas').getComponent('CanvasManager');
        canvas.allSlide();

        this.sound.playSE(3);
        this.sound.dicisionBGM();

        if (this.gameNode.active == false) {
            setTimeout(() => {
                this.gameNode.active = true;
                this.isStart = true;
                this.sound.playBGM();
            }, 300);
        }
        this.generateTime = 1.5;
    },

    gameOver() {
        cc.sys.localStorage.setItem('isDead', 1);
        this.isGameOver = true;
        this.stopAll();

        this.sound.stopBGM();
        this.sound.playSE(4);

        var score = cc.find('Canvas/ScoreManager').getComponent('ScoreManager');
        score.setScore();

        var camera = cc.find('Canvas/Camera');
        camera.active = true;

        setTimeout(() => {
            cc.director.loadScene('Game');
        }, 1300);
    },

    stopAll() {
        var items = cc.find('Canvas/_Game/Items').children;
        for (var i; i < items.length; i++) {
            items[i].getComponent('ItemManager').stopChildren();
        }
        var obstacles = cc.find('Canvas/_Game/Obstacles').children;
        if (obstacles.length > 0) {
            for (var i = 0; i < obstacles.length; i++) {
                obstacles[i].getComponent('ObstacleManager').stopMotion();
            }
        }
    },

    allAvoid() {
        var obstacles = cc.find('Canvas/_Game/Obstacles').children;
        if (obstacles.length > 0) {
            for (var i = 0; i < obstacles.length; i++) {
                obstacles[i].getComponent('ObstacleManager').avoid();
            }
        }
    },

    update(dt) {
        if (this.isStart && !this.isGameOver) {
            this.generateTime -= dt;
            if (this.generateTime < 0) {
                this.generateTime = Math.random() * (5 - 2.5) + 2.5;
                var GS = cc.find('Canvas/GameManager').getComponent('GenerateSystem');
                GS.generateObstacle();
            }
        }
    },
});
