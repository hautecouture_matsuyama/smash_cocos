
var Sound = cc.Class({
    name: 'Sound',
    properties: {
        url: cc.AudioClip,
    },
});

cc.Class({
    extends: cc.Component,

    properties: {

        //  0:コイン, 1:着地, 2:ヒット, 3:開始声, 4:ゲームオーバー声, 5:リロード声, 6:無敵解除, 7:スコア更新,
        //  8:決定音, 9:キャンセル音, 10:ロック音, 11:買い物

        sounds: {
            default: [],
            type: Sound,
        },

        backs: {
            default: [],
            type: Sound,
        },

        BGMLabel: cc.Label,
        prev: cc.Node,
        next: cc.Node,

        selectID: 0,

        bgmId: -1,
    },

    // onLoad () {},

    start() {
        var data = cc.sys.localStorage.getItem('No');
        if (data != null) {
            this.selectID = data;
            this.changeBGM();
        }
    },

    playSE(No, customEventData) {
        if (customEventData != null)
            No = Number(customEventData);

        cc.audioEngine.play(this.sounds[No].url);

    },

    playBGM() {
        this.bgmId = cc.audioEngine.play(this.backs[this.selectID].url, true);
    },

    stopBGM() {
        cc.audioEngine.stop(this.bgmId);
        this.bgmId = -1;
    },

    prevBGM() {
        if (this.selectID > 0) {
            this.selectID--;
            this.changeBGM();
        }
    },

    nextBGM() {
        if (this.selectID < this.backs.length - 1) {
            this.selectID++;
            this.changeBGM();
        }
    },

    changeBGM() {

        if (this.selectID == 0)
            this.prev.active = false;
        else
            this.prev.active = true;

        this.BGMLabel.string = 'MUSIC ' + this.selectID;

        if (this.selectID == this.backs.length - 1)
            this.next.active = false;
        else
            this.next.active = true;
    },

    dicisionBGM() {
        cc.sys.localStorage.setItem('No', this.selectID);
    }

    // update (dt) {},
});
