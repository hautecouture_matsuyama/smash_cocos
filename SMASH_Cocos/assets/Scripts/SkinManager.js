
var SoundManager = require('SoundManager');
var ScoreManager = require('ScoreManager');

var Skin = cc.Class({
    name: 'Skin',
    properties: {
        my: cc.Node,
        lock: cc.Node,
        rate: 0,
        bonusRate: 1,
        pos: cc.Vec2,
        prefab: cc.Prefab,
    },
});

cc.Class({
    extends: cc.Component,

    properties: {
        skins: {
            default: [],
            type: Skin,
        },

        sound: SoundManager,
        score: ScoreManager,

        player: cc.Sprite,

        skinNo: 0,      //次回のゲームのプレイヤーに割り当てるスキン番号
        selectNo: 0,    //選択しているスキン番号(ロックボタンをタップした時用)

        selectIcon: cc.Node,
        defaultParent: cc.Node,

        confirmationScreen: cc.Node,

        rateLabel: cc.Label,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        this.confirmIsReleaseAll();

        var data = cc.sys.localStorage.getItem('skinNo');
        if (data != null) {
            this.skinNo = data;

            this.change(data);

        }
    },

    dicisionSkin() {
        cc.sys.localStorage.setItem('skin', this.skinNo);

        cc.sys.localStorage.setItem('skinNo', this.skinNo);

        if (this.skins[this.skinNo].prefab != null) {
            var GS = cc.find('Canvas/GameManager').getComponent('GenerateSystem');
            GS.generatePlayerVisual(this.skins[this.skinNo].prefab);
        }
        this.score.setSkinRate(this.skins[this.skinNo].bonusRate);
    },

    //CustomEventDataには数値だけ入れること。
    select(event, customEventData) {
        var value = Number(customEventData);
        if (this.skinNo != value) {
            this.sound.playSE(8);

            this.change(value);
        }
    },

    change(value) {
        this.selectIcon.setPosition(this.skins[value].pos);

        this.skins[this.skinNo].my.parent = this.defaultParent;
        this.skins[this.skinNo].my.setPosition(this.skins[this.skinNo].pos);
        this.skinNo = value;
        this.skins[this.skinNo].my.parent = this.selectIcon;
        this.skins[this.skinNo].my.setPosition(cc.p(0, 0));
    },

    buyJudge(event, customEventData) {

        var currentPoint = this.score._userData.point;

        this.selectNo = Number(customEventData);

        if (this.skins[this.selectNo].rate <= currentPoint) {
            this.confirmationScreen.active = true;
            this.sound.playSE(8);
            this.rateLabel.string = 'Do you want to\n release new skin\n using ' + this.skins[this.selectNo].rate + 'pt?'
        }
        else {
            this.sound.playSE(10);
        }
    },

    skinRelease() {
        cc.sys.localStorage.setItem('Lock_' + String(this.selectNo), 1);
        this.skins[this.selectNo].lock.destroy();
        this.score.bought(this.skins[this.selectNo].rate);
        this.sound.playSE(11);
    },

    //各スキンが解放済みかどうか確認
    confirmIsReleaseAll() {
        for (var i = 1; i < this.skins.length; i++) {
            var isRelease = cc.sys.localStorage.getItem('Lock_' + String(i));
            if (isRelease != null) {
                if (isRelease == 1)
                    this.skins[i].lock.destroy();
            }
        }
    },



    // update (dt) {},
});
